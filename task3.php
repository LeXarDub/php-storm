<?php

function rewriteJsonFile(string $pathToJsonFile, string $key, $value): void {
   $json = file_get_contents($pathToJsonFile);
   $jsonArray = json_decode($json,true);
   $jsonArray[$key] = $value;
   $encoded=json_encode($jsonArray);
    file_put_contents($pathToJsonFile, $encoded);
}
