<?php

function createFileWithSum(string $pathToFiles): void
{
    if(!is_dir($pathToFiles)){
        return;
    }
        $file1 = file($pathToFiles . '/1.txt');
        $file2 = file($pathToFiles . '/2.txt');

        foreach ($file1 as $index => $one) {
            $two = $file2[$index];

            $three[$index] = $one + $two;
        }
        file_put_contents($pathToFiles . '/3.txt', implode(PHP_EOL, $three));
}